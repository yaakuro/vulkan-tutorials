/*
	Vulkan Framebuffer Tutorial Copyright (c) 2017 - 2018 Cengiz Terzibas (Yaakuro)

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.

*/

#include <vkf/vulkan_framework_windows.h>
#include <vkf/vulkan_framework.h>

#include <string>
#include <array>
#include <iostream>
#include <cassert>

std::string title("Vulkan Framebuffer Tutorial Copyright (c) 2017 - 2018 Cengiz Terzibas (Yaakuro)");

uint32_t width = 1280;
uint32_t height = 800;

VkInstance instance = VK_NULL_HANDLE;
VkDevice device = VK_NULL_HANDLE;
VkSurfaceKHR surface = VK_NULL_HANDLE;
VkSwapchainKHR swapchain = VK_NULL_HANDLE;

xcb_connection_t* connection = nullptr;
xcb_screen_t* screen = nullptr;
xcb_window_t window = 0;

struct SwapChainImages {
	VkImage image;
	VkImageView imageView;
};

std::vector<SwapChainImages> swapchainImageBuffers;

void exitProgram() {
	for(auto& swapchainImage : swapchainImageBuffers) {
		vkDestroyImageView(device, swapchainImage.imageView, nullptr);
	}
	swapchainImageBuffers.clear();

	if(VK_NULL_HANDLE != swapchain) {
		vkDestroySwapchainKHR(device, swapchain, nullptr);
		swapchain = VK_NULL_HANDLE;
	}

	if(VK_NULL_HANDLE != surface) {
		vkDestroySurfaceKHR(instance, surface, nullptr);
		surface = VK_NULL_HANDLE;
	}

	if(0 != window) {
		xcb_destroy_window(connection, window);
		window = 0;
	}
	if(nullptr != connection) {
		xcb_disconnect(connection);
		connection = nullptr;
	}

	if(VK_NULL_HANDLE != device) {
		vkDestroyDevice(device, nullptr);
		device = VK_NULL_HANDLE;
	}

	if(VK_NULL_HANDLE != instance) {
		vkDestroyInstance(instance, nullptr);
		instance = VK_NULL_HANDLE;
	}

	vkf::destroy();
}

int main(int argc, char **argv) {

	// Initialize Vulkan library dynamically.
	vkf::init();

	// Assigned function that will be called when we do exit.
	atexit(exitProgram);

	// ---------------------------------------------------------------------------
	// Create Vulkan Instance.
	//
	VkInstanceCreateInfo instanceCreateInfo {};
	instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;

	std::vector<const char*> extensionsName {VK_KHR_SURFACE_EXTENSION_NAME, VK_KHR_XCB_SURFACE_EXTENSION_NAME};
	instanceCreateInfo.ppEnabledExtensionNames = extensionsName.data();
	instanceCreateInfo.enabledExtensionCount = extensionsName.size();

	CHECK_VULKAN(vkCreateInstance(&instanceCreateInfo, nullptr, &instance));

	// -----------------------------------------------------------------------------------------

	// -------------------------------------------------------------------------
	// Get first the number of physical devices (how many GPU's).
	//
	uint32_t deviceCount = 0;
	CHECK_VULKAN(vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr));

	// Do the same but this time get the info for the physical devices too.
	std::vector<VkPhysicalDevice> physicalDevices(deviceCount);
	CHECK_VULKAN(vkEnumeratePhysicalDevices(instance, &deviceCount, physicalDevices.data()));

	// -------------------------------------------------------------------------

	//
	// Here we need to make sure that we get a device that supports rendering.
	// For now, lets assume it is the first device. (Bad assumption)
	//
	VkPhysicalDevice graphicsPhysicalDevice;

	uint32_t index = 0;
	uint32_t queueGraphicsIndex = 0;
	for(auto physicalDevice : physicalDevices) {

		VkPhysicalDeviceProperties physicalDeviceProperties = {};
		vkGetPhysicalDeviceProperties(physicalDevice, &physicalDeviceProperties);

		// Here we check for a GPU like one plugged into a PCI/E slot. (There are internal one, like
		// integrated into the CPU. So its up to you what to do here. For our example, its ok.
		//
		if(physicalDeviceProperties.deviceType != VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
			continue;
		}

		std::cout << "Found GPU: " << physicalDeviceProperties.deviceName << std::endl;

		uint32_t queueFamilyPropertyCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertyCount, nullptr);

		std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyPropertyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertyCount, queueFamilyProperties.data());

		for(auto& queueFamilyProperty : queueFamilyProperties) {
			if(queueFamilyProperty.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
				queueGraphicsIndex = index;
				graphicsPhysicalDevice = physicalDevice;
			}
			index++;
		}
	}

	// ---------------------------------------------------------------------------
	// Create Vulkan Device.
	//
	VkDeviceQueueCreateInfo deviceQueueInfo;
	deviceQueueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	deviceQueueInfo.queueFamilyIndex = queueGraphicsIndex;

	// Create only one queue
	float queuePriorities[] = { 1.0f };
	deviceQueueInfo.queueCount = 1;
	deviceQueueInfo.pQueuePriorities = queuePriorities;

	VkDeviceCreateInfo deviceCreateInfo {};
	deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceCreateInfo.queueCreateInfoCount = 1;
	deviceCreateInfo.pQueueCreateInfos = &deviceQueueInfo;

	std::vector<const char*> instanceExtensionsName {VK_KHR_SWAPCHAIN_EXTENSION_NAME};
	deviceCreateInfo.ppEnabledExtensionNames = instanceExtensionsName.data();
	deviceCreateInfo.enabledExtensionCount = instanceExtensionsName.size();

	CHECK_VULKAN(vkCreateDevice(graphicsPhysicalDevice, &deviceCreateInfo, nullptr, &device));

	// ---------------------------------------------------------------------------

	// ---------------------------------------------------------------------------
	// Create a window.
	//
	connection = xcb_connect(nullptr, nullptr);
	screen = xcb_setup_roots_iterator(xcb_get_setup(connection)).data;
	window = xcb_generate_id(connection);

	uint32_t values[2];
	uint32_t mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
	values[0] = screen->black_pixel;
	values[1] = XCB_EVENT_MASK_EXPOSURE       | XCB_EVENT_MASK_BUTTON_PRESS   |
	            XCB_EVENT_MASK_BUTTON_RELEASE | XCB_EVENT_MASK_POINTER_MOTION |
	            XCB_EVENT_MASK_ENTER_WINDOW   | XCB_EVENT_MASK_LEAVE_WINDOW   |
	            XCB_EVENT_MASK_KEY_PRESS      | XCB_EVENT_MASK_KEY_RELEASE;

	xcb_create_window(connection,                     // X11 connection.
	                  XCB_COPY_FROM_PARENT,          // Depth.
	                  window,                        // Window id.
	                  screen->root,                  // Parent window.
	                  0, 0,                          // X, Y position of the window on the screen.
	                  width, height,                      // Width, Height of the window.
	                  0,                             // Border width.
	                  XCB_WINDOW_CLASS_INPUT_OUTPUT, // Class type. (Input/Output for normal window)
	                  screen->root_visual,           // Visual.
	                  mask,                          // Masks.
	                  values);                       // Values.

	xcb_intern_atom_cookie_t cookie = xcb_intern_atom(connection, 1, 12, "WM_PROTOCOLS");
	xcb_intern_atom_reply_t* reply = xcb_intern_atom_reply(connection, cookie, 0);

	xcb_intern_atom_cookie_t cookie2 = xcb_intern_atom(connection, 0, 16, "WM_DELETE_WINDOW");
	xcb_intern_atom_reply_t* atom_wm_delete_window = xcb_intern_atom_reply(connection, cookie2, 0);

	xcb_change_property(connection, XCB_PROP_MODE_REPLACE,
	                    window, (*reply).atom, 4, 32, 1,
	                    &(*atom_wm_delete_window).atom);

	xcb_change_property(connection, XCB_PROP_MODE_REPLACE,
	                    window, XCB_ATOM_WM_NAME, XCB_ATOM_STRING, 8,
	                    title.size(), title.c_str());

	free(reply);

	// Map the window so we can see it.
	xcb_map_window(connection, window);

	// Flush the command queue for the X11 server.
	xcb_flush(connection);

	// Create Key symbols which will be used when converting key events into keysym.
	xcb_key_symbols_t* kss = xcb_key_symbols_alloc(connection);





	// ---------------------------------------------------------------------------
	// Create Vulkan Surface.
	//
	VkXcbSurfaceCreateInfoKHR xcbSurfaceCreateInfoKHR {};
	xcbSurfaceCreateInfoKHR.sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;
	xcbSurfaceCreateInfoKHR.connection = connection;
	xcbSurfaceCreateInfoKHR.window = window;
	CHECK_VULKAN(vkCreateXcbSurfaceKHR(instance, &xcbSurfaceCreateInfoKHR, nullptr, &surface));

	// ---------------------------------------------------------------------------


	// ---------------------------------------------------------------------------
	// Create Vulkan SwapChain.
	//

	VkSwapchainCreateInfoKHR swapchainCreateInfo {};
	swapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	swapchainCreateInfo.surface = surface;
	swapchainCreateInfo.presentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
	swapchainCreateInfo.imageExtent = {width, height};
	swapchainCreateInfo.queueFamilyIndexCount = queueGraphicsIndex;
	swapchainCreateInfo.presentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
	swapchainCreateInfo.imageColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
	swapchainCreateInfo.imageFormat = VK_FORMAT_R8G8B8A8_UNORM;
	swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	swapchainCreateInfo.minImageCount = 2;
	swapchainCreateInfo.imageArrayLayers = 1;
	swapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	swapchainCreateInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;

	CHECK_VULKAN(vkCreateSwapchainKHR(device, &swapchainCreateInfo, nullptr, &swapchain));

	// Now we have the SwaChain. With it we will have VkImages ready. To be able to use it
	// we will create the VkImageView for them.
	uint32_t imageCount;
	CHECK_VULKAN(vkGetSwapchainImagesKHR(device, swapchain, &imageCount, nullptr));

	std::vector<VkImage> swapchainImages(imageCount);
	CHECK_VULKAN(vkGetSwapchainImagesKHR(device, swapchain, &imageCount, swapchainImages.data()));

	swapchainImageBuffers.reserve(imageCount);
	swapchainImageBuffers.resize(imageCount);

	for(size_t idx = 0; idx < swapchainImageBuffers.size(); idx++) {
		swapchainImageBuffers[idx].image = swapchainImages[idx];

		VkImageViewCreateInfo imageViewCreateInfo {};
		imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		imageViewCreateInfo.image = swapchainImageBuffers[idx].image;
		imageViewCreateInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
		imageViewCreateInfo.components = {
			VK_COMPONENT_SWIZZLE_R,
			VK_COMPONENT_SWIZZLE_G,
			VK_COMPONENT_SWIZZLE_B,
			VK_COMPONENT_SWIZZLE_A
		};
		imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		imageViewCreateInfo.subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };

		CHECK_VULKAN(vkCreateImageView(device, &imageViewCreateInfo, nullptr, &swapchainImageBuffers[idx].imageView));
	}

	// ---------------------------------------------------------------------------
	// Get Vulkan framebuffer.
	//

	std::vector<VkFramebuffer> framebuffers(3);
	VkFramebufferCreateInfo framebufferCreateInfo {};
	framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	framebufferCreateInfo.pNext = nullptr;
	framebufferCreateInfo.flags = 0;
	framebufferCreateInfo.renderPass = VK_NULL_HANDLE; //renderPass;
	framebufferCreateInfo.width = width;
	framebufferCreateInfo.height = height;
	framebufferCreateInfo.layers = 1;
	framebufferCreateInfo.attachmentCount = (uint32_t)swapchainImageBuffers.size();

	std::vector<VkImageView> attachments;
	for(auto& attachment : swapchainImageBuffers) {
		attachments.push_back(attachment.imageView);
	}
	framebufferCreateInfo.pAttachments = attachments.data();

	vkCreateFramebuffer(device, &framebufferCreateInfo, nullptr, (VkFramebuffer*)framebuffers.data());

	// ---------------------------------------------------------------------------

	// ---------------------------------------------------------------------------
	// Get Vulkan queue.
	//
	VkQueue graphicsQueue = VK_NULL_HANDLE;
	VkQueue presentQueue = VK_NULL_HANDLE;
	vkGetDeviceQueue(device, queueGraphicsIndex, 0, &graphicsQueue);
	vkGetDeviceQueue(device, queueGraphicsIndex, 0, &presentQueue);

	// ---------------------------------------------------------------------------
	// Create Vulkan command pool.
	//
	VkCommandPool commandPool = VK_NULL_HANDLE;
	VkCommandPoolCreateInfo commandPoolCreateInfo {};
	commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	commandPoolCreateInfo.pNext = nullptr;
	commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
	commandPoolCreateInfo.queueFamilyIndex = queueGraphicsIndex;

	CHECK_VULKAN(vkCreateCommandPool(device, &commandPoolCreateInfo, nullptr, &commandPool));

	std::vector<VkCommandBuffer> drawCommandBuffers(3);
	VkCommandBufferAllocateInfo commandBufferAllocateInfo {};
	commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocateInfo.commandPool = commandPool;
	commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	commandBufferAllocateInfo.commandBufferCount = 3;

	CHECK_VULKAN(vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, drawCommandBuffers.data()));


	VkSemaphore presentSemaphore = VK_NULL_HANDLE;
	VkSemaphoreCreateInfo presentCompleteSemaphoreCreateInfo {};
	presentCompleteSemaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	vkCreateSemaphore(device, &presentCompleteSemaphoreCreateInfo, nullptr, &presentSemaphore);


	VkSemaphore renderSemaphore = VK_NULL_HANDLE;
	VkSemaphoreCreateInfo renderCompleteSemaphoreCreateInfo {};
	renderCompleteSemaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	vkCreateSemaphore(device, &renderCompleteSemaphoreCreateInfo, nullptr, &renderSemaphore);

	// ---------------------------------------------------------------------------

	//
	VkCommandBufferBeginInfo commandBufferBeginInfo {};
	commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

	std::array<VkClearValue, 2> clearValues {};
	clearValues[0].color = {{0.0f, 0.0f, 0.0f, 1.0f}};
	clearValues[1].depthStencil = { 1.0f, 0 };

	VkRenderPassBeginInfo renderPassBeginInfo {};
	renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassBeginInfo.renderPass = VK_NULL_HANDLE;//renderpass;
	renderPassBeginInfo.renderArea.offset.x = 0;
	renderPassBeginInfo.renderArea.offset.y = 0;
	renderPassBeginInfo.renderArea.extent.width = width;
	renderPassBeginInfo.renderArea.extent.height = height;
	renderPassBeginInfo.clearValueCount = clearValues.size();
	renderPassBeginInfo.pClearValues = clearValues.data();

	for(size_t i = 0; i < drawCommandBuffers.size(); ++i) {

		renderPassBeginInfo.framebuffer = framebuffers[i];

		vkBeginCommandBuffer(drawCommandBuffers[i], &commandBufferBeginInfo);
		{
			vkCmdBeginRenderPass(drawCommandBuffers[i], &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
			{
				VkViewport viewport {};
				viewport.width = width;
				viewport.height = height;
				viewport.minDepth = 0.0f;
				viewport.maxDepth = 0.0f;
				vkCmdSetViewport(drawCommandBuffers[i], 0, 1, &viewport);

				VkRect2D scissor {};
				scissor.extent.width = width;
				scissor.extent.height = height;

				vkCmdSetScissor(drawCommandBuffers[i], 0, 1, &scissor);
			}
			vkCmdEndRenderPass(drawCommandBuffers[i]);
		}
		vkEndCommandBuffer(drawCommandBuffers[i]);
	}


	// Start main window event loop.
	for(;;) {
		xcb_generic_event_t* event;
		event = xcb_poll_for_event(connection);
		if(nullptr != event) {
			switch(event->response_type & ~0x80) {
				case XCB_EXPOSE: {
					xcb_expose_event_t *ev = (xcb_expose_event_t *)event;
					break;
				}
				case XCB_KEY_PRESS: {
					xcb_key_press_event_t* ev = (xcb_key_press_event_t*)event;
					xcb_keysym_t ks = xcb_key_symbols_get_keysym(kss, ev->detail, 0);

					if(XK_Escape == ks) {
						goto end;
					}

					break;
				}
				case XCB_KEY_RELEASE: {
					xcb_key_release_event_t* ev = (xcb_key_release_event_t*)event;
					break;
				}
				case XCB_CLIENT_MESSAGE: {
					if((*(xcb_client_message_event_t*)event).data.data32[0] == (*atom_wm_delete_window).atom) {
						goto end;
					}
				}
				break;

				case XCB_DESTROY_NOTIFY: {
					goto end;
				}
				break;
			}
		}

		uint32_t currentBuffer = 0;
		VkResult result = vkAcquireNextImageKHR(device, swapchain, std::numeric_limits<uint64_t>::max(), presentSemaphore, VK_NULL_HANDLE, &currentBuffer);
		if(VK_SUCCESS == result || VK_SUBOPTIMAL_KHR == result) {

			//
			// Submit the commands.
			//
			VkPipelineStageFlags waitStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			VkSubmitInfo submitInfo {};
			submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
			submitInfo.pWaitDstStageMask = &waitStageMask;
			submitInfo.pWaitSemaphores = &presentSemaphore;
			submitInfo.waitSemaphoreCount = 1;
			submitInfo.pSignalSemaphores = &renderSemaphore;
			submitInfo.signalSemaphoreCount = 1;
			submitInfo.pCommandBuffers = &drawCommandBuffers[currentBuffer];
			submitInfo.commandBufferCount = 1;

			vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);

			//
			// When ready present the framebuffer.
			//
			VkPresentInfoKHR present {};
			present.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
			present.swapchainCount = 1;
			present.waitSemaphoreCount = 1;
			present.pWaitSemaphores = &renderSemaphore;
			present.pSwapchains = &swapchain;
			present.pImageIndices = &currentBuffer;

			VkResult result = vkQueuePresentKHR(presentQueue, &present);
			if(result == VK_SUCCESS || result == VK_SUBOPTIMAL_KHR) {
				result = vkQueueWaitIdle(presentQueue);
			}
		}
	}

end:

	//
	// Free all resources for xcb we used so far.
	//
	xcb_key_symbols_free(kss);

	// ---------------------------------------------------------------------------

	return EXIT_SUCCESS;
}
