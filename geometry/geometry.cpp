/*
	Vulkan Geometry Shader Tutorial Copyright (c) 2017 - 2018 Cengiz Terzibas (Yaakuro)

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.

*/

#include <vkf/vulkan_framework_windows.h>
#include <vkf/vulkan_framework.h>

#include <tm/tm.h>

struct UniformBufferStruct {
	tmath::mat4 projectionMatrix;
	tmath::mat4 modelMatrix;
};

UniformBufferStruct ufb;

void updateUniformBuffers(vkf::UniformBuffer* uniformBuffer, uint32_t width, uint32_t height) {
	tmath::frustum(-0.41f, 0.41f, -0.31f, 0.31f, 1.0f, 100.0f, ufb.projectionMatrix);

	static float angle = 0.0f;
	tmath::mat4 rx, ry, trans;
	tmath::rotate_x(-25.0f, rx);
	tmath::rotate_y(angle, ry);
	tmath::translate(0.0f, 0.0f, -30.0f, trans);
	ufb.modelMatrix = trans * rx * ry;

	angle += 0.01f;

	uniformBuffer->upload((uint8_t*)&ufb, vkf::BufferSize(sizeof(UniformBufferStruct)));
}


int main(int argc, char **argv) {

	vkf::init();

	vkf::Windows* window = vkf::createWindows();
	window->init("Vulkan Geometry Shader Tutorial Copyright (c) 2017 - 2018 Cengiz Terzibas (Yaakuro)", 800, 600);

	vkf::Instance* instance = vkf::createInstance();
	instance->init();

	vkf::Device* device = instance->createDevice();
	device->init();

	vkf::Surface* surface = device->createSurface(window);
	surface->init();

	vkf::SwapChain* swapChain = surface->createSwapChain();
	swapChain->init();

	vkf::CommandPool* commandPool = device->createCommandPool();
	commandPool->init();

	vkf::CommandBuffers* commandBuffers = commandPool->createCommandBuffers();
	commandBuffers->init(swapChain->getSwapImages().size());

	vkf::VertexShader* vertexShader = device->createVertexShader();
	vertexShader->load(vkf::FileName("shaders/geometry.vert.spv"));

	vkf::GeometryShader* geometryShader = device->createGeometryShader();
	geometryShader->load(vkf::FileName("shaders/geometry.geom.spv"));

	vkf::FragmentShader* fragmentShader = device->createFragmentShader();
	fragmentShader->load(vkf::FileName("shaders/geometry.frag.spv"));

	struct Vertex {
		float x, y, z;
	};

	const int particles = 1024*100;

	// create a galaxy like distribution of points
	std::vector<Vertex> vertices(particles) ;

	for(auto i = 0; i < particles; ++i) {
		int arm = 3*(std::rand()/float(RAND_MAX));
		float alpha = 1.0f/(0.1f+std::pow(std::rand()/float(RAND_MAX),0.7f))-1/1.1f;
		float radius = 5.0f * alpha;
		alpha += arm * 2.0f * M_PI / 3.0f;

		vertices[i].x = radius*std::sin(alpha);
		vertices[i].y = 0;
		vertices[i].z = radius*std::cos(alpha);

		vertices[i].x += (4.0f-0.2*alpha)*(2-(std::rand()/float(RAND_MAX)+std::rand()/float(RAND_MAX) + std::rand()/float(RAND_MAX)+std::rand()/float(RAND_MAX)));
		vertices[i].y += (2.0f-0.1*alpha)*(2-(std::rand()/float(RAND_MAX)+std::rand()/float(RAND_MAX) + std::rand()/float(RAND_MAX)+std::rand()/float(RAND_MAX)));
		vertices[i].z += (4.0f-0.2*alpha)*(2-(std::rand()/float(RAND_MAX)+std::rand()/float(RAND_MAX) + std::rand()/float(RAND_MAX)+std::rand()/float(RAND_MAX)));
	}


	vkf::VertexBuffer* vertexBuffer = device->createVertexBuffer();
	vertexBuffer->addElement(vkf::BindingPoint(0), vkf::LocationPoint(0), VK_FORMAT_R32G32B32_SFLOAT, offsetof(Vertex, x));
	vertexBuffer->init(vkf::BufferSize(vertices.size() * sizeof(Vertex)), vkf::BufferStride(sizeof(Vertex)));
	vertexBuffer->upload((uint8_t*)vertices.data(), vkf::BufferSize(vertices.size() * sizeof(Vertex)));

	vkf::UniformBuffer* uniformBuffer = device->createUniformBuffer();
	uniformBuffer->init(vkf::BufferSize(sizeof(UniformBufferStruct)));

	// ---------------------------------------------------------------------------


	// ---------------------------------------------------------------------------
	// Prepare resource information used by the shaders.
	// Now we tell vulkan at which binding point we like to have which buffers, samplers etc in which shader stage.
	//
	const vkf::BindingPoint uniformBufferBindingPoint(0);

	vkf::DescriptorSetLayout* descriptorSetLayout = device->createDescriptorSetLayout();
	descriptorSetLayout->add(uniformBufferBindingPoint, vkf::DescriptorCount(1), VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_GEOMETRY_BIT);
	descriptorSetLayout->init();

	// Lets describe what kind of binding points we're going to have.
	vkf::DescriptorPool* descriptorPool = device->createDescriptorPool();
	descriptorPool->add(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER);
	descriptorPool->init();

	// Now we assign the real objects to the binding points.
	vkf::DescriptorSet* descriptorSet = descriptorPool->createDescriptorSet();
	descriptorSet->bind(uniformBufferBindingPoint, uniformBuffer);
	descriptorSet->init(descriptorSetLayout);
	// ---------------------------------------------------------------------------

	vkf::Pipeline* pipeline = device->createPipeline();
	pipeline->addVertexState(vertexBuffer);
	pipeline->addShaderModule(vertexShader);
	pipeline->addShaderModule(geometryShader);
	pipeline->addShaderModule(fragmentShader);
	pipeline->setUsedTopology(VK_PRIMITIVE_TOPOLOGY_POINT_LIST);
	pipeline->setEnableDepthStencil(VK_FALSE);

	pipeline->setBlendEnable(VK_TRUE);
	pipeline->setBlendSrcColorFactor(VK_BLEND_FACTOR_ONE);
	pipeline->setBlendDstColorFactor(VK_BLEND_FACTOR_ONE);

	pipeline->init(descriptorSetLayout, swapChain->getRenderPass(), surface->getExtent2D());

	vkf::Queue* renderQueue = device->createQueue();
	renderQueue->init(0);

	vkf::Queue* presentQueue = device->createQueue();
	presentQueue->init(1);

	vkf::Semaphore* presentSemaphore = device->createSemaphore();
	presentSemaphore->init();

	vkf::Semaphore* renderSemaphore = device->createSemaphore();
	renderSemaphore->init();

	// Fill command buffer.
	std::array<VkClearValue, 2> clearValues {};
	clearValues[0].color = {{0.0f, 0.0f, 0.0f, 1.0f}};
	clearValues[1].depthStencil = { 1.0f, 0 };

	for(uint32_t index = 0; index < swapChain->getSwapImages().size(); index++) {
		vkf::CommandBuffer& commandBuffer = *commandBuffers->getCommandBuffer(index);
		{
			vkf::CommandBufferScope commandBufferScope(&commandBuffer);
			{
				vkf::RenderPassScope renderPassScope(&commandBuffer, swapChain, index, clearValues);

				// Set viewport.
				VkViewport viewport {};
				viewport.width = window->getWidth();
				viewport.height = window->getHeight();
				viewport.minDepth = 0.0f;
				viewport.maxDepth = 1.0f;
				vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

				// Set scissor.
				VkRect2D scissor {};
				scissor.extent = swapChain->getExtent2D();
				vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

				// bind descriptors.
				vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, *pipeline);

				VkDeviceSize offsets[1] = { 0 };
				vkCmdBindVertexBuffers(commandBuffer, 0, 1, *vertexBuffer, offsets);

				vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->getPipelineLayout(), 0, 1, *descriptorSet, 0, nullptr);

				vkCmdDraw(commandBuffer, vertices.size(), 1, 0, 0 );

			}
		}
	}

	window->show();
	while(window->pollEvents()) {

		// Update uniform buffers.
		updateUniformBuffers(uniformBuffer, window->getWidth(), window->getHeight());

		// Handle SwapBuffers.
		uint32_t currentBuffer;
		VkResult result = swapChain->nextImage(currentBuffer, presentSemaphore);
		if(VK_SUCCESS == result || VK_SUBOPTIMAL_KHR == result) {

			//
			// Submit the commands.
			//
			renderQueue->submit(commandBuffers->getCommandBuffer(currentBuffer), VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, renderSemaphore, presentSemaphore);

			//
			// When ready present the framebuffer.
			//
			VkResult result = swapChain->present(presentQueue, currentBuffer, renderSemaphore);
			if(result == VK_SUCCESS || result == VK_SUBOPTIMAL_KHR) {
				result = vkQueueWaitIdle(*presentQueue);
			}
		}
	}

	presentSemaphore->destroy();
	delete presentSemaphore;

	renderSemaphore->destroy();
	delete renderSemaphore;

	renderQueue->destroy();
	delete renderQueue;

	presentQueue->destroy();
	delete presentQueue;

	uniformBuffer->destroy();
	delete uniformBuffer;

	vertexBuffer->destroy();
	delete vertexBuffer;

	vertexShader->destroy();
	delete vertexShader;

	fragmentShader->destroy();
	delete fragmentShader;

	pipeline->destroy();
	delete pipeline;

	descriptorSet->destroy();
	delete descriptorSet;

	descriptorSetLayout->destroy();
	delete descriptorSetLayout;

	descriptorPool->destroy();
	delete descriptorPool;

	commandBuffers->destroy();
	delete commandBuffers;

	commandPool->destroy();
	delete commandPool;

	swapChain->destroy();
	delete swapChain;

	surface->destroy();
	delete surface;

	device->destroy();
	delete device;

	instance->destroy();
	delete instance;

	vkf::destroy();

	return EXIT_SUCCESS;
}
