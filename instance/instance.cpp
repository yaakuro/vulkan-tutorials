/*
	Vulkan Instance Tutorial Copyright (c) 2017 - 2018 Cengiz Terzibas (Yaakuro)

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.

*/

#include <vkf/vulkan_framework_windows.h>
#include <vkf/vulkan_framework.h>

VkInstance instance = VK_NULL_HANDLE;

void exitProgram() {

	if(VK_NULL_HANDLE != instance) {
		vkDestroyInstance(instance, nullptr);
		instance = VK_NULL_HANDLE;
	}

	vkf::destroy();
}

int main(int argc, char **argv) {

	// Initialize Vulkan library dynamically.
	vkf::init();

	// Assigned function that will be called when we do exit.
	atexit(exitProgram);

	// ---------------------------------------------------------------------------
	// Create Vulkan Instance.
	//

	VkInstanceCreateInfo instanceCreateInfo {};
	instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;

	CHECK_VULKAN(vkCreateInstance(&instanceCreateInfo, nullptr, &instance));

	return EXIT_SUCCESS;
}
