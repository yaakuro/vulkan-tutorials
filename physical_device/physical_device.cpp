/*
	Vulkan Physical Device Tutorial Copyright (c) 2017 - 2018 Cengiz Terzibas (Yaakuro)

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.

*/

#include <vkf/vulkan_framework_windows.h>
#include <vkf/vulkan_framework.h>

VkInstance instance = VK_NULL_HANDLE;
VkDevice device = VK_NULL_HANDLE;

void exitProgram() {

	if(VK_NULL_HANDLE != device) {
		vkDestroyDevice(device, nullptr);
		device = VK_NULL_HANDLE;
	}

	if(VK_NULL_HANDLE != instance) {
		vkDestroyInstance(instance, nullptr);
		instance = VK_NULL_HANDLE;
	}

	vkf::destroy();
}

int main(int argc, char **argv) {

	// Initialize Vulkan library dynamically.
	vkf::init();

	// Assigned function that will be called when we do exit.
	atexit(exitProgram);

	// ---------------------------------------------------------------------------
	// Create Vulkan Instance.
	//

	VkInstanceCreateInfo instanceCreateInfo {};
	instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;

	CHECK_VULKAN(vkCreateInstance(&instanceCreateInfo, nullptr, &instance));

	// -----------------------------------------------------------------------------------------


	// -------------------------------------------------------------------------
	// Get first the number of physical devices.
	//
	uint32_t deviceCount = 0;

	// Using nullptr in the 3'rd parameter tells the function just to fill out the deviceCount.
	CHECK_VULKAN(vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr));

	// Do the same but this time get the info for the physical devices too.
	std::vector<VkPhysicalDevice> physicalDevices(deviceCount);
	CHECK_VULKAN(vkEnumeratePhysicalDevices(instance, &deviceCount, physicalDevices.data()));

	// -------------------------------------------------------------------------


	// -------------------------------------------------------------------------
	// Here we need to make sure that we get a device that supports rendering.
	//
	VkPhysicalDevice graphicsPhysicalDevice;

	uint32_t index = 0;
	uint32_t queueGraphicsIndex = 0;
	for(auto physicalDevice : physicalDevices) {

		VkPhysicalDeviceProperties physicalDeviceProperties = {};
		vkGetPhysicalDeviceProperties(physicalDevice, &physicalDeviceProperties);

		// Here we check for a GPU like one plugged into a PCI/E slot. (There are internal one, like
		// integrated into the CPU. So its up to you what to do here. For our example, its ok.
		//
		if(physicalDeviceProperties.deviceType != VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
			continue;
		}

		std::cout << "Found GPU: " << physicalDeviceProperties.deviceName << std::endl;

		uint32_t queueFamilyPropertyCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertyCount, nullptr);

		std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyPropertyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertyCount, queueFamilyProperties.data());

		for(auto& queueFamilyProperty : queueFamilyProperties) {
			if(queueFamilyProperty.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
				queueGraphicsIndex = index;
				graphicsPhysicalDevice = physicalDevice;
			}
			index++;
		}
	}

	// -------------------------------------------------------------------------


	// ---------------------------------------------------------------------------
	// Create Vulkan Device.
	//
	VkDeviceQueueCreateInfo deviceQueueInfo {};
	deviceQueueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	deviceQueueInfo.queueFamilyIndex = queueGraphicsIndex;

	// Create only one queue
	float queuePriorities[] = { 1.0f };
	deviceQueueInfo.queueCount = 1;
	deviceQueueInfo.pQueuePriorities = queuePriorities;

	VkDeviceCreateInfo deviceCreateInfo {};
	deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceCreateInfo.queueCreateInfoCount = 1;
	deviceCreateInfo.pQueueCreateInfos = &deviceQueueInfo;

	CHECK_VULKAN(vkCreateDevice(graphicsPhysicalDevice, &deviceCreateInfo, nullptr, &device));

	// ---------------------------------------------------------------------------

	return EXIT_SUCCESS;
}
