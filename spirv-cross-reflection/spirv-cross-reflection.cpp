/*
	Vulkan Cube Tutorial Copyright (c) 2017 - 2018 Cengiz Terzibas (Yaakuro)

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.

*/

#include <vkf/vulkan_framework_windows.h>
#include <vkf/vulkan_framework.h>
#include <spirv_cfg.hpp>
#include <spirv_cross.hpp>

#include <tm/tm.h>
#include <string.h>

struct TransformationsUniformBufferStruct {
	tmath::mat4 projectionMatrix;
	tmath::mat4 modelMatrix;
};

struct LightUniformBufferStruct {
	tmath::vec4 lightPosition;
	tmath::vec4 lightColor;
	tmath::vec4 lightAmbient;
};

TransformationsUniformBufferStruct ufb;
LightUniformBufferStruct ufb2;

void updateUniformBuffers(std::map<std::string, vkf::UniformBuffer*>& uniformBuffers, uint32_t width, uint32_t height) {
	tmath::frustum(-0.41f, 0.41f, -0.31f, 0.31f, 1.0f, 100.0f, ufb.projectionMatrix);

	static float angle = 0.0f;
	tmath::mat4 rx, ry, trans;
	tmath::rotate_x(angle, rx);
	tmath::rotate_y(angle, ry);
	tmath::translate(0.0f, 0.0f, -7.0f, trans);
	ufb.modelMatrix = trans * rx * ry;

	angle += 0.01f;

	uniformBuffers["Transformations"]->upload((uint8_t*)&ufb, vkf::BufferSize(sizeof(TransformationsUniformBufferStruct)));

	ufb2.lightPosition = tmath::vec4(0.0f, -10.0f, 0.0f, 0.0f);
	ufb2.lightColor = tmath::vec4(1.0f, 0.6f, 0.0f, 0.0f);
	ufb2.lightAmbient = tmath::vec4(0.0f, 0.03f, 0.03f, 0.0f);
	uniformBuffers["Light"]->upload((uint8_t*)&ufb2, vkf::BufferSize(sizeof(LightUniformBufferStruct)));
}


int main(int argc, char **argv) {

	vkf::init();

	vkf::Windows* window = vkf::createWindows();
	window->init("Vulkan Cube Tutorial Copyright (c) 2017 - 2018 Cengiz Terzibas (Yaakuro)", 800, 600);

	vkf::Instance* instance = vkf::createInstance();
	instance->init(window->getTitle());

	vkf::Device* device = instance->createDevice();
	device->init();

	vkf::Surface* surface = device->createSurface(window);
	surface->init();

	vkf::SwapChain* swapChain = surface->createSwapChain();
	swapChain->init();

	vkf::CommandPool* commandPool = device->createCommandPool();
	commandPool->init();

	vkf::CommandBuffers* commandBuffers = commandPool->createCommandBuffers();
	commandBuffers->init(swapChain->getSwapImages().size());

	vkf::VertexShader* vertexShader = device->createVertexShader();
	vertexShader->load(vkf::FileName("shaders/spirv-cross-reflection.vert.spv"));

	vkf::FragmentShader* fragmentShader = device->createFragmentShader();
	fragmentShader->load(vkf::FileName("shaders/spirv-cross-reflection.frag.spv"));

	struct Vertex {
		float x, y, z, w;
		float nx, ny, nz;
		float r, g, b, a;
	};

	std::vector<Vertex> vertices = {
		{-1.0f,-1.0f,-1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f,-1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ 1.0f, 1.0f,-1.0f, 1.0f,  0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f,-1.0f,-1.0f, 1.0f,  0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f, 1.0f,-1.0f, 1.0f,  0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ 1.0f,-1.0f, 1.0f, 1.0f,  0.0f, -1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f,-1.0f,-1.0f, 1.0f,  0.0f, -1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ 1.0f,-1.0f,-1.0f, 1.0f,  0.0f, -1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ 1.0f, 1.0f,-1.0f, 1.0f,  0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ 1.0f,-1.0f,-1.0f, 1.0f,  0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f,-1.0f,-1.0f, 1.0f,  0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{-1.0f,-1.0f,-1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f, 1.0f,-1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ 1.0f,-1.0f, 1.0f, 1.0f,  0.0f,-1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f,-1.0f, 1.0f, 1.0f,  0.0f,-1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f,-1.0f,-1.0f, 1.0f,  0.0f,-1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{-1.0f, 1.0f, 1.0f, 1.0f,  0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f,-1.0f, 1.0f, 1.0f,  0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ 1.0f,-1.0f, 1.0f, 1.0f,  0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ 1.0f, 1.0f, 1.0f, 1.0f,  1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ 1.0f,-1.0f,-1.0f, 1.0f,  1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ 1.0f, 1.0f,-1.0f, 1.0f,  1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ 1.0f,-1.0f,-1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ 1.0f,-1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ 1.0f, 1.0f,-1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f, 1.0f,-1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f, 1.0f,-1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ 1.0f,-1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f}
	};

	vkf::VertexBuffer* vertexBuffer = device->createVertexBuffer();
	vertexBuffer->addElement(vkf::BindingPoint(0), vkf::LocationPoint(0), VK_FORMAT_R32G32B32A32_SFLOAT, offsetof(Vertex, x));
	vertexBuffer->addElement(vkf::BindingPoint(0), vkf::LocationPoint(1), VK_FORMAT_R32G32B32_SFLOAT, offsetof(Vertex, nx));
	vertexBuffer->addElement(vkf::BindingPoint(0), vkf::LocationPoint(2), VK_FORMAT_R32G32B32A32_SFLOAT, offsetof(Vertex, r));

	vertexBuffer->init(vkf::BufferSize(vertices.size() * sizeof(Vertex)), vkf::BufferStride(sizeof(Vertex)));
	vertexBuffer->upload((uint8_t*)vertices.data(), vkf::BufferSize(vertices.size() * sizeof(Vertex)));

	std::map<std::string, vkf::UniformBuffer*> uniformBuffers;
	uniformBuffers["Transformations"] = device->createUniformBuffer();
	uniformBuffers["Transformations"]->init(vkf::BufferSize(sizeof(TransformationsUniformBufferStruct)));
	uniformBuffers["Light"] = device->createUniformBuffer();
	uniformBuffers["Light"]->init(vkf::BufferSize(sizeof(LightUniformBufferStruct)));



	// ---------------------------------------------------------------------------
	// Prepare resource information used by the shaders.
	// Now we tell Vulkan at which binding point we like to have which buffers, samplers etc in which shader stage.
	// This time we will use SPIRV-Cross to get the binding points.
	//

	vkf::DescriptorPool* descriptorPool = device->createDescriptorPool();
	descriptorPool->add(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER);
	descriptorPool->init();

	spirv_cross::Compiler vs((vertexShader->getBinary()));
	spirv_cross::ShaderResources resources = vs.get_shader_resources();

	// The DescriptorSetLayout describes how the buffers we want to bind and use in the shaders look like.
	vkf::DescriptorSetLayout* descriptorSetLayout = device->createDescriptorSetLayout();

	// DescriptorSet assigns the real buffers to the binding points.
	vkf::DescriptorSet* descriptorSet = descriptorPool->createDescriptorSet();

	vkf::BindingPoint uniformBufferBindingPoint(0);
	vkf::DescriptorCount descriptorCount(1);

	for(auto& resource : resources.uniform_buffers) {
		unsigned int binding = vs.get_decoration(resource.id, spv::DecorationBinding);
		std::cout << resource.name << " at binding: " << binding << std::endl;

		// We will bind a uniform buffer to binding: 0 and have only 1 and will be used only in the vertex shader stage.
		uniformBufferBindingPoint = vkf::BindingPoint(binding);

		descriptorSetLayout->add(uniformBufferBindingPoint, descriptorCount, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT);
		descriptorSet->bind(uniformBufferBindingPoint, uniformBuffers[resource.name]);
	}

	descriptorSetLayout->init();
	descriptorSet->init(descriptorSetLayout);

	// ---------------------------------------------------------------------------

	vkf::Pipeline* pipeline = device->createPipeline();
	pipeline->addVertexState(vertexBuffer);
	pipeline->addShaderModule(vertexShader);
	pipeline->addShaderModule(fragmentShader);
	pipeline->init(descriptorSetLayout, swapChain->getRenderPass(), surface->getExtent2D());

	vkf::Queue* renderQueue = device->createQueue();
	renderQueue->init(0);

	vkf::Queue* presentQueue = device->createQueue();
	presentQueue->init(1);

	vkf::Semaphore* presentSemaphore = device->createSemaphore();
	presentSemaphore->init();

	vkf::Semaphore* renderSemaphore = device->createSemaphore();
	renderSemaphore->init();


	// Fill command buffer.
	std::array<VkClearValue, 2> clearValues {};
	clearValues[0].color = {{0.0f, 0.0f, 0.0f, 1.0f}};
	clearValues[1].depthStencil = { 1.0f, 0 };

	for(uint32_t index = 0; index < swapChain->getSwapImages().size(); index++) {
		vkf::CommandBuffer& commandBuffer = *commandBuffers->getCommandBuffer(index);
		{
			vkf::CommandBufferScope commandBufferScope(&commandBuffer);
			{
				vkf::RenderPassScope renderPassScope(&commandBuffer, swapChain, index, clearValues);

				// Set viewport.
				VkViewport viewport {};
				viewport.width = surface->getExtent2D().width;
				viewport.height = surface->getExtent2D().height;
				viewport.minDepth = 0.0f;
				viewport.maxDepth = 1.0f;
				vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

				// Set scissor.
				VkRect2D scissor {};
				scissor.extent = swapChain->getExtent2D();
				vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

				// bind descriptors.
				vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, *pipeline);

				VkDeviceSize offsets[1] = { 0 };
				vkCmdBindVertexBuffers(commandBuffer, 0, 1, *vertexBuffer, offsets);

				vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->getPipelineLayout(), 0, 1, *descriptorSet, 0, nullptr);

				vkCmdDraw(commandBuffer, 36, 1, 0, 0);

			}
		}
	}

	window->show();
	while(window->pollEvents()) {

		// Update uniform buffers.
		updateUniformBuffers(uniformBuffers, window->getWidth(), window->getHeight());

		// Handle SwapBuffers.
		uint32_t currentBuffer;
		VkResult result = swapChain->nextImage(currentBuffer, presentSemaphore);
		if(VK_SUCCESS == result || VK_SUBOPTIMAL_KHR == result) {

			//
			// Submit the commands.
			//
			renderQueue->submit(commandBuffers->getCommandBuffer(currentBuffer), VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, renderSemaphore, presentSemaphore);

			//
			// When ready present the framebuffer.
			//
			VkResult result = swapChain->present(presentQueue, currentBuffer, renderSemaphore);
			if(result == VK_SUCCESS || result == VK_SUBOPTIMAL_KHR) {
				result = vkQueueWaitIdle(*presentQueue);
			}
		}
	}


	presentSemaphore->destroy();
	delete presentSemaphore;

	renderSemaphore->destroy();
	delete renderSemaphore;

	renderQueue->destroy();
	delete renderQueue;

	presentQueue->destroy();
	delete presentQueue;

	for(auto& uniformBuffer : uniformBuffers) {
		uniformBuffer.second->destroy();
		delete uniformBuffer.second;
	}

	vertexBuffer->destroy();
	delete vertexBuffer;

	vertexShader->destroy();
	delete vertexShader;

	fragmentShader->destroy();
	delete fragmentShader;

	pipeline->destroy();
	delete pipeline;

	descriptorSet->destroy();
	delete descriptorSet;

	descriptorSetLayout->destroy();
	delete descriptorSetLayout;

	descriptorPool->destroy();
	delete descriptorPool;

	commandBuffers->destroy();
	delete commandBuffers;

	commandPool->destroy();
	delete commandPool;

	swapChain->destroy();
	delete swapChain;

	surface->destroy();
	delete surface;

	device->destroy();
	delete device;

	instance->destroy();
	delete instance;

	vkf::destroy();

	return EXIT_SUCCESS;
}
