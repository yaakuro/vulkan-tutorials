/*
	Vulkan Surface Tutorial Copyright (c) 2017 - 2018 Cengiz Terzibas (Yaakuro)

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.

*/

#include <vkf/vulkan_framework_windows.h>
#include <vkf/vulkan_framework.h>

#include <string>
#include <array>
#include <iostream>
#include <cassert>

std::string title("Vulkan Surface Tutorial Copyright (c) 2017 - 2018 Cengiz Terzibas (Yaakuro)");

uint32_t width = 640;
uint32_t height = 480;

VkInstance instance = VK_NULL_HANDLE;
VkDevice device = VK_NULL_HANDLE;
VkSurfaceKHR surface = VK_NULL_HANDLE;

xcb_connection_t* connection = nullptr;
xcb_screen_t* screen = nullptr;
xcb_window_t window = 0;

void exitProgram() {

	if(VK_NULL_HANDLE != surface) {
		vkDestroySurfaceKHR(instance, surface, nullptr);
		surface = VK_NULL_HANDLE;
	}

	if(0 != window) {
		xcb_destroy_window(connection, window);
		window = 0;
	}
	if(nullptr != connection) {
		xcb_disconnect(connection);
		connection = nullptr;
	}

	if(VK_NULL_HANDLE != device) {
		vkDestroyDevice(device, nullptr);
		device = VK_NULL_HANDLE;
	}

	if(VK_NULL_HANDLE != instance) {
		vkDestroyInstance(instance, nullptr);
		instance = VK_NULL_HANDLE;
	}

	vkf::destroy();
}

int main(int argc, char **argv) {

	// Initialize Vulkan library dynamically.
	vkf::init();

	// Assigned function that will be called when we do exit.
	atexit(exitProgram);

	// ---------------------------------------------------------------------------
	// Create Vulkan Instance.
	//
	VkApplicationInfo applicationInfo {};
	applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	applicationInfo.apiVersion = VK_MAKE_VERSION(1, 0, 0);

	VkInstanceCreateInfo instanceCreateInfo {};
	instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instanceCreateInfo.pApplicationInfo = &applicationInfo;

	CHECK_VULKAN(vkCreateInstance(&instanceCreateInfo, nullptr, &instance));

	// -----------------------------------------------------------------------------------------


	// -------------------------------------------------------------------------
	// Get first the number of physical devices (how many GPU's).
	//
	uint32_t deviceCount = 0;
	CHECK_VULKAN(vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr));

	// Do the same but this time get the info for the physical devices too.
	std::vector<VkPhysicalDevice> physicalDevices(deviceCount);
	CHECK_VULKAN(vkEnumeratePhysicalDevices(instance, &deviceCount, physicalDevices.data()));

	// -------------------------------------------------------------------------

	//
	// Here we need to make sure that we get a device that supports rendering.
	// For now, lets assume it is the first device. (Bad assumption)
	//
	VkPhysicalDevice physicalDevice = physicalDevices[0];

	uint32_t queueGraphicsIndex = 0;
	for(auto& physicalDevice : physicalDevices) {
		uint32_t queueFamilyPropertyCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertyCount, nullptr);

		std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyPropertyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertyCount, queueFamilyProperties.data());

		for(auto& queueFamilyProperty : queueFamilyProperties) {
			if(queueFamilyProperty.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
				break;
			}
		}
		queueGraphicsIndex++;
	}

	// ---------------------------------------------------------------------------
	// Create Vulkan Device.
	//
	VkDeviceQueueCreateInfo deviceQueueInfo;
	deviceQueueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	deviceQueueInfo.queueFamilyIndex = queueGraphicsIndex;

	// Create only one queue
	float queuePriorities[] = { 1.0f };
	deviceQueueInfo.queueCount = 1;
	deviceQueueInfo.pQueuePriorities = queuePriorities;

	VkDeviceCreateInfo deviceCreateInfo {};
	deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceCreateInfo.queueCreateInfoCount = 1;
	deviceCreateInfo.pQueueCreateInfos = &deviceQueueInfo;

	std::vector<const char*> instanceExtensionsName {VK_KHR_SWAPCHAIN_EXTENSION_NAME};
	deviceCreateInfo.ppEnabledExtensionNames = instanceExtensionsName.data();
	deviceCreateInfo.enabledExtensionCount = instanceExtensionsName.size();

	CHECK_VULKAN(vkCreateDevice(physicalDevice, &deviceCreateInfo, nullptr, &device));

	// ---------------------------------------------------------------------------

	// ---------------------------------------------------------------------------
	// Create a window.
	//
	connection = xcb_connect(nullptr, nullptr);
	screen = xcb_setup_roots_iterator(xcb_get_setup(connection)).data;
	window = xcb_generate_id(connection);

	uint32_t values[2];
	uint32_t mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
	values[0] = screen->black_pixel;
	values[1] = XCB_EVENT_MASK_EXPOSURE       | XCB_EVENT_MASK_BUTTON_PRESS   |
	            XCB_EVENT_MASK_BUTTON_RELEASE | XCB_EVENT_MASK_POINTER_MOTION |
	            XCB_EVENT_MASK_ENTER_WINDOW   | XCB_EVENT_MASK_LEAVE_WINDOW   |
	            XCB_EVENT_MASK_KEY_PRESS      | XCB_EVENT_MASK_KEY_RELEASE;

	xcb_create_window(connection,                     // X11 connection.
	                  XCB_COPY_FROM_PARENT,          // Depth.
	                  window,                        // Window id.
	                  screen->root,                  // Parent window.
	                  0, 0,                          // X, Y position of the window on the screen.
	                  width, height,                      // Width, Height of the window.
	                  0,                             // Border width.
	                  XCB_WINDOW_CLASS_INPUT_OUTPUT, // Class type. (Input/Output for normal window)
	                  screen->root_visual,           // Visual.
	                  mask,                          // Masks.
	                  values);                       // Values.

	xcb_intern_atom_cookie_t cookie = xcb_intern_atom(connection, 1, 12, "WM_PROTOCOLS");
	xcb_intern_atom_reply_t* reply = xcb_intern_atom_reply(connection, cookie, 0);

	xcb_intern_atom_cookie_t cookie2 = xcb_intern_atom(connection, 0, 16, "WM_DELETE_WINDOW");
	xcb_intern_atom_reply_t* atom_wm_delete_window = xcb_intern_atom_reply(connection, cookie2, 0);

	xcb_change_property(connection, XCB_PROP_MODE_REPLACE,
	                    window, (*reply).atom, 4, 32, 1,
	                    &(*atom_wm_delete_window).atom);

	xcb_change_property(connection, XCB_PROP_MODE_REPLACE,
	                    window, XCB_ATOM_WM_NAME, XCB_ATOM_STRING, 8,
	                    title.size(), title.c_str());

	free(reply);

	// Map the window so we can see it.
	xcb_map_window(connection, window);

	// Flush the command queue for the X11 server.
	xcb_flush(connection);

	// Create Key symbols which will be used when converting key events into keysym.
	xcb_key_symbols_t* kss = xcb_key_symbols_alloc(connection);

	// Start main window event loop.
	for(;;) {
		xcb_generic_event_t* event;
		event = xcb_poll_for_event(connection);
		if(nullptr != event) {
			switch(event->response_type & ~0x80) {
				case XCB_EXPOSE: {
					xcb_expose_event_t *ev = (xcb_expose_event_t *)event;
					break;
				}
				case XCB_KEY_PRESS: {
					xcb_key_press_event_t* ev = (xcb_key_press_event_t*)event;
					xcb_keysym_t ks = xcb_key_symbols_get_keysym(kss, ev->detail, 0);

					if(XK_Escape == ks) {
						goto end;
					}

					break;
				}
				case XCB_KEY_RELEASE: {
					xcb_key_release_event_t* ev = (xcb_key_release_event_t*)event;
					break;
				}
				case XCB_CLIENT_MESSAGE: {
					if((*(xcb_client_message_event_t*)event).data.data32[0] == (*atom_wm_delete_window).atom) {
						goto end;
					}
				}
				break;

				case XCB_DESTROY_NOTIFY: {
					goto end;
				}
				break;
			}
		}

		vkDeviceWaitIdle(device);
	}

end:

	//
	// Free all resources for xcb we used so far.
	//
	xcb_key_symbols_free(kss);

	// ---------------------------------------------------------------------------


	// ---------------------------------------------------------------------------
	// Create Vulkan Surface.
	//
	VkXcbSurfaceCreateInfoKHR xcbSurfaceCreateInfoKHR {};
	xcbSurfaceCreateInfoKHR.sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;
	xcbSurfaceCreateInfoKHR.connection = connection;
	xcbSurfaceCreateInfoKHR.window = window;
	CHECK_VULKAN(vkCreateXcbSurfaceKHR(instance, &xcbSurfaceCreateInfoKHR, nullptr, &surface));

	// ---------------------------------------------------------------------------

	return EXIT_SUCCESS;
}
