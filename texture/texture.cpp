/*
	Vulkan Texture Tutorial Copyright (c) 2017 - 2018 Cengiz Terzibas (Yaakuro)

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.

*/

#include <vkf/vulkan_framework_windows.h>
#include <vkf/vulkan_framework.h>
#include <lodepng/lodepng.h>

#include <tm/tm.h>

struct UniformBufferStruct {
	tmath::mat4 projectionMatrix;
	tmath::mat4 modelMatrix;
	tmath::mat4 viewMatrix;
};

UniformBufferStruct ufb;

void updateUniformBuffers(vkf::UniformBuffer* uniformBuffer, uint32_t width, uint32_t height) {
	tmath::frustum(-0.41f, 0.41f, -0.31f, 0.31f, 1.0f, 100.0f, ufb.projectionMatrix);

	tmath::identity(ufb.viewMatrix);

	static float angle = 0.0f;
	tmath::mat4 rx, ry, trans;
	tmath::rotate_x(0.0f, rx);
	tmath::rotate_y(angle, ry);
	tmath::translate(0.0f, 0.0f, -3.0f, trans);
	ufb.modelMatrix = trans * rx * ry;

	angle += 0.01f;

	uniformBuffer->upload((uint8_t*)&ufb, vkf::BufferSize(sizeof(UniformBufferStruct)));
}


int main(int argc, char **argv) {

	vkf::init();

	vkf::Windows* window = vkf::createWindows();
	window->init("Vulkan Texture Tutorial Copyright (c) 2017 - 2018 Cengiz Terzibas (Yaakuro)", 800, 600);

	vkf::Instance* instance = vkf::createInstance();
	instance->init(window->getTitle());

	vkf::Device* device = instance->createDevice();
	device->init();

	vkf::Surface* surface = device->createSurface(window);
	surface->init();

	vkf::SwapChain* swapChain = surface->createSwapChain();
	swapChain->init();

	vkf::CommandPool* commandPool = device->createCommandPool();
	commandPool->init();

	vkf::CommandBuffers* commandBuffers = commandPool->createCommandBuffers();
	commandBuffers->init(swapChain->getSwapImages().size());

	vkf::VertexShader* vertexShader = device->createVertexShader();
	vertexShader->load(vkf::FileName("shaders/texture.vert.spv"));

	vkf::FragmentShader* fragmentShader = device->createFragmentShader();
	fragmentShader->load(vkf::FileName("shaders/texture.frag.spv"));

	struct Vertex {
		float x, y, z;
		float u, v;
	};

	std::vector<Vertex> vertices = {
		{-0.5f, -0.5f, 0.0f,   0.0f, 1.0f},
		{-0.5f,  0.5f, 0.0f,   0.0f, 0.0f},
		{ 0.5f,  0.5f, 0.0f,   1.0f, 0.0f},
		{ 0.5f, -0.5f, 0.0f,   1.0f, 1.0f}
	};

	vkf::VertexBuffer* vertexBuffer = device->createVertexBuffer();
	vertexBuffer->addElement(vkf::BindingPoint(0), vkf::LocationPoint(0), VK_FORMAT_R32G32B32_SFLOAT, offsetof(Vertex, x));
	vertexBuffer->addElement(vkf::BindingPoint(0), vkf::LocationPoint(1), VK_FORMAT_R32G32_SFLOAT, offsetof(Vertex, u));
	vertexBuffer->init(vkf::BufferSize(vertices.size() * sizeof(Vertex)), vkf::BufferStride(sizeof(Vertex)));
	vertexBuffer->upload((uint8_t*)vertices.data(), vkf::BufferSize(vertices.size() * sizeof(Vertex)));

	std::vector<uint32_t> indices = {
		0, 1, 2,
		2, 3, 0,
	};

	vkf::IndexBuffer* indexBuffer = device->createIndexBuffer();
	indexBuffer->init(indices.size(), vkf::BufferSize(indices.size() * sizeof(uint32_t)));
	indexBuffer->upload((uint8_t*)indices.data(), vkf::BufferSize(indices.size() * sizeof(uint32_t)));

	vkf::UniformBuffer* uniformBuffer = device->createUniformBuffer();
	uniformBuffer->init(vkf::BufferSize(sizeof(UniformBufferStruct)));


	// ---------------------------------------------------------------------------
	// Prepare the texture.
	//

	// Load texture.
	std::vector<unsigned char> png, image;
	unsigned int width, height;

	lodepng::load_file(png, std::string("textures/texture01.png"));

	unsigned int error = lodepng::decode(image, width, height, png);
	if(error) {
		std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;
		return 1;
	}

	// lodepng has flipped images.
	for(unsigned int y = 0; y < height/2; y++) {
		for(unsigned int x = 0; x < width * 4; x++) {
			unsigned char tmp = image[x + 4 * width * y];
			image[x + 4 * width * y] = image[x + 4 * width * (height - 1 - y)];
			image[x + 4 * width * (height - 1 - y)] = tmp;
		}
	}

	// We will use this staging buffer to copy the image data to the texture.
	// Because vkf::Texture resides by default in the GPU memory we can't just use map to upload data.
	vkf::StagingBuffer* stagingBuffer = device->createStagingBuffer();
	stagingBuffer->init(vkf::BufferSize(image.size()));
	stagingBuffer->upload(image.data(), vkf::BufferSize(image.size()), 0);

	vkf::Texture2D texture(device, VK_FORMAT_R8G8B8A8_UNORM, width, height);
	texture.init();

	vkf::TextureSampler textureSampler(device);
	textureSampler.setMinFilter(VK_FILTER_LINEAR);
	textureSampler.setMagFilter(VK_FILTER_LINEAR);
	textureSampler.init();

	// ---------------------------------------------------------------------------
	// Copy staging buffer into the texture.
	//
	vkf::CommandBuffer* copyCommandBuffer = commandPool->createCommandBuffer();
	copyCommandBuffer->init();

	vkf::Queue* copyQueue = device->createQueue();
	copyQueue->init(2);
	{
		vkf::CommandBufferScope copyCommandBufferScope(copyCommandBuffer);
		vkf::setImageLayout(copyCommandBuffer, texture.getImage(), VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

		VkBufferImageCopy region {};
		region.bufferOffset = 0;
		region.bufferRowLength = 0;
		region.bufferImageHeight = 0;

		region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		region.imageSubresource.mipLevel = 0;
		region.imageSubresource.baseArrayLayer = 0;
		region.imageSubresource.layerCount = 1;

		region.imageOffset = {0, 0, 0};
		region.imageExtent = {
			width,
			height,
			1
		};
		vkCmdCopyBufferToImage(*copyCommandBuffer, *stagingBuffer, texture.getImage(), texture.getImageLayout(), 1, &region);
		vkf::setImageLayout(copyCommandBuffer, texture.getImage(), VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
	}
	copyQueue->submit(copyCommandBuffer);
	// ---------------------------------------------------------------------------

	// ---------------------------------------------------------------------------


	// ---------------------------------------------------------------------------
	// Prepare resource information used by the shaders.
	// Now we tell vulkan at which binding point we like to have which buffers, samplers etc in which shader stage.
	//
	const vkf::BindingPoint uniformBufferBindingPoint(0);
	const vkf::BindingPoint samplerBindingPoint(1);

	vkf::DescriptorSetLayout* descriptorSetLayout = device->createDescriptorSetLayout();
	descriptorSetLayout->add(uniformBufferBindingPoint, vkf::DescriptorCount(1), VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT);
	descriptorSetLayout->add(samplerBindingPoint, vkf::DescriptorCount(1), VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT);
	descriptorSetLayout->init();

	// Lets describe what kind of binding points we're going to have.
	vkf::DescriptorPool* descriptorPool = device->createDescriptorPool();
	descriptorPool->add(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER);
	descriptorPool->add(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER);
	descriptorPool->init();

	// Now we assign the real objects to the binding points.
	vkf::DescriptorSet* descriptorSet = descriptorPool->createDescriptorSet();
	descriptorSet->bind(uniformBufferBindingPoint, uniformBuffer);
	descriptorSet->bind(samplerBindingPoint, &texture, &textureSampler);
	descriptorSet->init(descriptorSetLayout);
	// ---------------------------------------------------------------------------

	vkf::Pipeline* pipeline = device->createPipeline();
	pipeline->addVertexState(vertexBuffer);
	pipeline->addShaderModule(vertexShader);
	pipeline->addShaderModule(fragmentShader);
	pipeline->init(descriptorSetLayout, swapChain->getRenderPass(), surface->getExtent2D());

	vkf::Queue* renderQueue = device->createQueue();
	renderQueue->init(0);

	vkf::Queue* presentQueue = device->createQueue();
	presentQueue->init(1);

	vkf::Semaphore* presentSemaphore = device->createSemaphore();
	presentSemaphore->init();

	vkf::Semaphore* renderSemaphore = device->createSemaphore();
	renderSemaphore->init();

	// Fill command buffer.
	std::array<VkClearValue, 2> clearValues {};
	clearValues[0].color = {{0.0f, 0.0f, 0.0f, 1.0f}};
	clearValues[1].depthStencil = { 1.0f, 0 };

	for(uint32_t index = 0; index < swapChain->getSwapImages().size(); index++) {
		vkf::CommandBuffer& commandBuffer = *commandBuffers->getCommandBuffer(index);
		{
			vkf::CommandBufferScope commandBufferScope(&commandBuffer);
			{
				vkf::RenderPassScope renderPassScope(&commandBuffer, swapChain, index, clearValues);

				// Set viewport.
				VkViewport viewport {};
				viewport.width = surface->getExtent2D().width;
				viewport.height = surface->getExtent2D().height;
				viewport.minDepth = 0.0f;
				viewport.maxDepth = 1.0f;
				vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

				// Set scissor.
				VkRect2D scissor {};
				scissor.extent = swapChain->getExtent2D();
				vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

				// bind descriptors.
				vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, *pipeline);

				VkDeviceSize offsets[1] = { 0 };
				vkCmdBindVertexBuffers(commandBuffer, 0, 1, *vertexBuffer, offsets);

				vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->getPipelineLayout(), 0, 1, *descriptorSet, 0, nullptr);

				vkCmdBindIndexBuffer(commandBuffer, *indexBuffer, 0, VK_INDEX_TYPE_UINT32);
				vkCmdDrawIndexed(commandBuffer, indexBuffer->getNumberOfIndicies(), 1, 0, 0, 1);

			}
		}
	}

	window->show();
	while(window->pollEvents()) {

		// Update uniform buffers.
		updateUniformBuffers(uniformBuffer, window->getWidth(), window->getHeight());

		// Handle SwapBuffers.
		uint32_t currentBuffer;
		VkResult result = swapChain->nextImage(currentBuffer, presentSemaphore);
		if(VK_SUCCESS == result || VK_SUBOPTIMAL_KHR == result) {

			//
			// Submit the commands.
			//
			renderQueue->submit(commandBuffers->getCommandBuffer(currentBuffer), VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, renderSemaphore, presentSemaphore);

			//
			// When ready present the framebuffer.
			//
			VkResult result = swapChain->present(presentQueue, currentBuffer, renderSemaphore);
			if(result == VK_SUCCESS || result == VK_SUBOPTIMAL_KHR) {
				result = vkQueueWaitIdle(*presentQueue);
			}
		}
	}

	copyCommandBuffer->destroy();
	delete copyCommandBuffer;

	presentSemaphore->destroy();
	delete presentSemaphore;

	renderSemaphore->destroy();
	delete renderSemaphore;

	copyQueue->destroy();
	delete copyQueue;

	renderQueue->destroy();
	delete renderQueue;

	presentQueue->destroy();
	delete presentQueue;

	uniformBuffer->destroy();
	delete uniformBuffer;

	indexBuffer->destroy();
	delete indexBuffer;

	vertexBuffer->destroy();
	delete vertexBuffer;

	vertexShader->destroy();
	delete vertexShader;

	fragmentShader->destroy();
	delete fragmentShader;

	pipeline->destroy();
	delete pipeline;

	descriptorSet->destroy();
	delete descriptorSet;

	descriptorSetLayout->destroy();
	delete descriptorSetLayout;

	descriptorPool->destroy();
	delete descriptorPool;

	commandBuffers->destroy();
	delete commandBuffers;

	commandPool->destroy();
	delete commandPool;

	swapChain->destroy();
	delete swapChain;

	surface->destroy();
	delete surface;

	device->destroy();
	delete device;

	instance->destroy();
	delete instance;

	vkf::destroy();

	return EXIT_SUCCESS;
}
